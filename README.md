This repo is a WIP.

It contains some snippets and experiments to get [PrometheusSink](https://github.com/banzaicloud/spark-metrics/blob/master/PrometheusSink.md) working with our infra.

# Getting started

We provide dockerized Spark and Pushgateway services.

Start Pushgateway with 
```bash
docker compose up pushgateway
```

Start a one worker Spark cluster with:
```bash
docker compose up spark-master spark-worker
```

Submit a demo application with
```
docker compose up spark-client
```

Metrics should be available at `http://localhost:9091/`. Metrics reporting can be customized by modifing 
the `spark-metrics.conf` file.

# Manual setup

The following section describe how to manually setup Spark and PrometheusSink.

## Shipping metrics 

`spark-metrics` binaries for Spark 2.4.x + Scala 2.11 are at
https://gitlab.wikimedia.org/gmodena/spark-metrics/-/packages. 
```
REPOSITORIES=https://gitlab.wikimedia.org/api/v4/projects/139/packages/maven
WMF_SPARK_METRICS=com.banzaicloud:spark-metrics_2.11:2.4.5-1.0.0-wmf
PACKAGES=io.prometheus:simpleclient:0.12.0,io.prometheus:simpleclient_dropwizard:0.12.0,io.prometheus:simpleclient_pushgateway:0.12.0,io.dropwizard.metrics:metrics-core:3.1.2,${WMF_SPARK_METRICS}

spark-shell --master "local[2]" \
    --conf spark.metrics.conf=spark-metrics.conf \
    --repositories ${REPOSITORIES} \
    --packages ${PACKAGES}
```

## Spark
The quickest way to setup a local spark cluster is by using a python/conda venv
```
python3 -m venv venv
source venv/bin/activate
pip install pyspark==2.4
```

This requires JDK8 and Python 3.7.

# Custom metrics

`custom-metrics` provides examples of two strategies to ship custom metrics:
* Piggyback on Spark Source/Sink API. Spark 3.x introduces support for custom metric reporting with a [plugin API]().
* Bypass Spark's MetricsRegistry and ship directly to PushGateway.
