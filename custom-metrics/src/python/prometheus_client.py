from pyspark.sql import SparkSession
from prometheus_client import CollectorRegistry, Gauge, push_to_gateway


if __name__ == "__main__":
    spark = SparkSession.builder.getOrCreate()
    app_id = spark.sparkContext.getConf().get("spark.app.id")
    # Prometheus example from https://github.com/prometheus/client_python#exporting-to-a-pushgateway
    registry = CollectorRegistry()
    g = Gauge(
        "job_last_success_unixtime",
        "Last time a batch job successfully finished",
        registry=registry,
    )
    g.set_to_current_time()
    push_to_gateway("localhost:9091", job=app_id, registry=registry)
