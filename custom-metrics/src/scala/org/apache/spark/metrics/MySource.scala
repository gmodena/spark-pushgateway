// Modified from https://kb.databricks.com/metrics/spark-metrics.html
//
package org.apache.spark.metrics.source

import com.codahale.metrics.{Counter, Histogram, MetricRegistry}
import org.apache.spark.SparkEnv
import org.apache.spark.sql.SparkSession

class MySource extends Source {
  override val sourceName: String = "MyMetric"
  override val metricRegistry: MetricRegistry = new MetricRegistry

  val MySourceHist: Histogram = metricRegistry.histogram(MetricRegistry.name("mySourceHist"))
  val MySourceCounter: Counter = metricRegistry.counter(MetricRegistry.name("mySourceCounter"))
}

object MyMetricSourceJob extends App {
  
  val spark: SparkSession = SparkSession
    .builder
    .getOrCreate()

  val source: MyMetricSource = new MyMetricSource

  SparkEnv.get.metricsSystem.registerSource(source)
  (1 to 100).foreach(_ => source.MySourceHist.update(1L))
  (1 to 100).foreach(_ => source.MySourceCounter.inc(1L))
  SparkEnv.get.metricsSystem.report
  spark.stop()
}
